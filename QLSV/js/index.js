function hienThiThongTin() {
  var maSV = document.getElementById("txtMaSV").value;

  var tenSV = document.getElementById("txtTenSV").value;

  var loaiSV = document.getElementById("loaiSV").value;

  var diemToan = document.getElementById("txtDiemToan").value * 1;

  var diemVan = document.getElementById("txtDiemVan").value * 1;

  // gán giá trị cho object

  var sv = {
    ma: maSV,
    ten: tenSV,
    diemToan: diemToan,
    diemVan: diemVan,
    loai: loaiSV,
    tinhDTB: function () {
      return (this.diemToan + this.diemVan) / 2;
    },
    xepLoai: function () {
      var dtb = this.tinhDTB();
      if (dtb >= 5) {
        return "Đạt";
      } else {
        return "Rớt";
      }
    },
  };
  // show sv
  console.log("sv: ", sv.tinhDTB(), sv.xepLoai());
}

console.log("sv: ", sv);
