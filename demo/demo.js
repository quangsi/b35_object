//  array : thường chứa dữ liệu cùng loại
// object: thường chứa dữ liệu khác nhau, dùng để miu tả 1 đối tượng ( nhân viên, xe ô tô, nhân vật game, cho chó,...)

var dogName = "bull";
var dogAge = 2;
var dogGender = "male";
// key:value
var dog1 = {
  name: "bull",
  age: 2,
  gender: "male",
  score: 8,
};

const dog2 = {
  // property
  name: "milo",
  age: 1,
  gender: "female",
  score: 10,
  // method
  bark: function () {
    console.log("meo meo meo " + this.name);
  },
};

dog2.bark();

dog2.age = 5;
//  lấy ra giá trị từ 1 key của ojbect
console.log("dog2.name: ", dog2.name);
// update value của key
dog2.name = "kiki";
console.log("dog2.name: ", dog2.name);
// pass by value
var username = 1;
var friend = username;
friend = 0;

console.log("username: ", username);
// username
// pass by reference
var dog3 = dog2;
console.log("dog2: ", dog2);
console.log("dog3: ", dog3);
console.log("updated");
dog3.score = 1;
console.log("dog2: ", dog2);
console.log("dog3: ", dog3);

// dynamic key

var user = {
  name: "Alice",
  age: 2,
};
// [dog1, dog2];
user["name"] = "Bob";
// user.name ="bob"
var key = "name";
user[key] = "Tom";
key = "age";
// user.age =100
user[key] = 100;

console.log("user: ", user);

var dogList = [dog1, dog2];

console.log("dogList[0].name: ", dogList[0].name);
console.log("dogList: ", dogList);
console.log(`









`);

// array vs object

//  lưu danh sách sdt : array [] ~ index ~ có thứ tự

// lưu thông tin của 1 nhân viên : object {} ~ key ~ ko có thứ tự
