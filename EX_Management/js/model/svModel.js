function SinhVien(_ma, _ten, _email, _matKhau, _diemLy, _diemToan, _diemHoa) {
  this.ma = _ma;
  this.ten = _ten;
  this.email = _email;
  this.matKhau = _matKhau;
  this.diemLy = _diemLy;
  this.diemToan = _diemToan;
  this.diemHoa = _diemHoa;

  this.tinhDTB = function () {
    var dtb = (this.diemToan * 1 + this.diemHoa * 1 + this.diemLy * 1) / 3;
    return dtb.toFixed(1);
  };
}
